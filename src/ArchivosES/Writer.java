package ArchivosES;

import java.io.*; 
/**
 * Estructura de Escritura de archivos
 * 
 * @author Ignacio del Barrio 
 */
public class Writer { 
  private FileWriter fichero; 
  private BufferedWriter bw; 
  
  /**
   * Prepara el archivo para escritura
   * @param ruta Ruta del Archivo
   */
  public Writer(String ruta){ 
    //TODO - constructor 
    try 
    { 
      fichero = new FileWriter(ruta); 
      bw = new BufferedWriter(this.fichero); 
    } 
    catch (IOException ex) 
    { 
      System.out.println("Error al abrir el archivo"); 
      try 
      { //si se produjo un error intento cerrar el archivo 
        if (null != fichero) 
        fichero.close();     
      } 
      catch (Exception e2) 
      { 
        System.out.println("Error al cerrar el archivo"); 
      } 
    }
  } 
  
  /**
   * Escribe una linea en el archivo
   */
  public void write(String texto){ 
    //TODO  - escribir 
    try 
    {
       bw.write(texto); 
    } 
    catch (IOException ex) 
    { 
       System.out.println("Error al escribir el archivo"); 
    } 
  } 
  
  /**
   * Cierra el archivo
   */
  public void cerrarArchivo(){ 
    //TODO - cerrar archivo 
    try 
    { 
       bw.flush(); 
       fichero.close(); 
    } 
    catch (IOException ex) 
    { 
       System.out.println("Error al cerrar el archivo"); 
    } 
  } 
}