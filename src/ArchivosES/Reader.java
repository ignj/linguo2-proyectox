package ArchivosES;

import java.io.*; 
/**
 * Estructura de Lectura de archivos
 * 
 * @author Ignacio del Barrio 
 */
public class Reader { 
  private FileReader fichero; 
  private BufferedReader br; 
  
  /**
   * Prepara el archivo para lectura
   * @param ruta Ruta del Archivo
   */
  public Reader(String ruta){ 
    //TODO - constructor 
    try 
    { 
        fichero = new FileReader (new File(ruta)); 
        br = new BufferedReader(fichero); 
    } 
    catch (IOException ex) 
    { 
        System.out.println("Error al abrir al archivo"); 
        try 
        { //si se produjo un error intento cerrar el archivo 
            if (null != fichero) 
            fichero.close(); 
        } 
        catch (Exception e2) 
        { 
            System.out.println("Error al cerrar al archivo"); 
        } 
    }
  } 
  /**
   * Lee una linea del archivo
   * @return String con la linea de texto
   */
  public String readln(){ 
    //TODO  - leer una linea 
    String salida = ""; 
    String linea;
    try 
    {
       while((linea=br.readLine())!=null)
       salida += linea+"\n";
    } 
    catch (IOException ex) 
    { 
       System.out.println("Error al leer el archivo"); 
    } 
    return salida;
  } 
  
  /**
   * Cierra el archivo
   */
  public void cerrarArchivo(){ 
    //TODO - cerrar archivo 
    try 
    { 
       fichero.close(); 
    } 
    catch (IOException ex) 
    { 
       System.out.println("Error al cerrar el archivo"); 
    } 
  } 
} 