package Logica.Entidades;

import java.awt.Color;
import java.awt.Graphics;

import Logica.Textures.SpriteManager;

/**
 * Entidad que representa un camino de pintura de un piso a otro adyacente.
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public class Pincelada extends Entidad{

	public static final int NORTE = 0, ESTE = 1, SUR = 2, OESTE = 3;
	protected int orientacion;
	protected Piso piso1;
	protected Piso piso2;
	
	private Pincelada(String ref, int x, int y) {
		super(ref, x, y);
		// TODO Auto-generated constructor stub
	}
	
	public Pincelada(Piso origen, Piso destino, int direccion, int orientacion, Color color) {
		super("pincelada.png", 0, 0);
		int x = origen.getX();
		int y = origen.getY();
		piso1 = origen;
		piso2 = destino;
		this.orientacion = orientacion;
		if(orientacion == 3 || orientacion == 1)
			sprite = SpriteManager.get().getSprite("pincelada2.png");
		if(direccion > 0){
			switch (orientacion) {
			case 0://NORTE
				{
					y = destino.getY();
				}break;
			case 3://OESTE
				{
					x = destino.getX();
				}break;
			}
		}else if(direccion < 0){
			switch (orientacion) {
			case 2://SUR
				{
					y = destino.getY();
				}break;
			case 1://ESTE
				{
					x = destino.getX();
				}break;
			}
		}
		setX(x);
		setY(y);
		//System.out.println("Nueva pincelada");
		setColor(color);
	}
	
	/**
	 * Cambia el Color de la pincelada
	 * 
	 * @param color Color nuevo
	 */
	public void setColor(Color color){
		sprite.colorImage(color.getRed(), color.getGreen(), color.getBlue());
	}

	/**
	 * Comprueba si la pincelada pinta un camino de Piso1 a Piso2 o
	 * viceversa.
	 * 
	 * @param piso1 Piso
	 * @param piso2 Piso
	 * @returns Verdadero si la pincelada pertenece a ambos pisos
	 */
	public boolean pertenece(Piso piso1, Piso piso2) {
		return (this.piso1 == piso1 && this.piso2 == piso2) || 
				(this.piso2 == piso1 && this.piso1 == piso2);
	}
	

}
