package Logica.Entidades;

import Logica.Mundo;


/**
 * Entidad que representa al Robot Militar controlado por el Jugador.
 * A diferencia del Robot (Civil) este tiene la capacidad de ejecutar Disparos.
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public class Militar extends Robot {
	
	/** Cantidad de Balas disponibles del Robot es Atributo Especial */
	
	/**
	 * Constructor
	 */
	public Militar(Mundo g, Piso actual, String ref, int x,int y, String icon) {
		super(g, actual, ref, x, y, icon);
		accionEspecial = 10;
		nafta = 100;
		tieneAccionEspecial = true;
	}
	
	/**
	 * Metodos
	 */
	
	/**
	 * Redefine la Accion Especial del Robot Civil.
	 * El Militar es capaz de disparar.
	 * 
	 */
	public void hazTuGracia() {
		disparar();
	}
	
	/**
	 * Instancia un disparo capaz de da�ar objetos destructibles.
	 * 
	 * @returns disparo La Bala disparada en caso de tener Municion disponible.
	 */
	public void disparar() {
		if(accionEspecial > 0) {
			accionEspecial--;
			Bala disparo = new Bala(game,"disparo.png",(int)x+10,(int)y+10, 300);
			switch (orientacion) {
			case NORTE: disparo.setVerticalMovement(-disparo.getMoveSpeed());
				break;
			case SUR: disparo.setVerticalMovement(disparo.getMoveSpeed());
				break;
			case ESTE: disparo.setHorizontalMovement(disparo.getMoveSpeed());
				break;
			case OESTE: disparo.setHorizontalMovement(-disparo.getMoveSpeed());
				break;
			}
			game.addInstanciaBala(disparo);
		}else{
			System.out.println("Sin municion!");
		}
	}
	
}
