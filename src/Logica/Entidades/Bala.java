package Logica.Entidades;

import Logica.Mundo;


/**
 * Entidad que repesenta un disparo realizado por el Robot Militar
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public class Bala extends Entidad {

	/** Referencia al Mundo */
	protected Mundo game;
	/** Verdadero si el proyectil ya ha golpeado algo */
	protected boolean used = false;

	
	/**
	 * Crea un nuevo disparo del Jugador
	 * 
	 * @param game El Mundo en el que el disparo fue creado
	 * @param sprite El Sprite que representa este proyectil
	 * @param x Posicion inicial en x
	 * @param y Posicion inicial en y
	 */
	public Bala(Mundo game,String sprite,int x,int y, double ms) {
		super(sprite,x,y);
		
		this.game = game;
		moveSpeed = ms;
	}

	/**
	 * Mueve este proyectil basado en el tiempo transcurrido
	 * 
	 * @param delta El tiempo transcurrido desde la ultima vez que se movio
	 */
	public void move(long delta) {
		x += (delta * dx) / 1000;
		y += (delta * dy) / 1000;
		me.setBounds((int) x,(int) y,sprite.getWidth(),sprite.getHeight());
		
		// Si esta fuera de la pantalla, eliminarlo **TERMINAR para Entrega Final**
		//if (y < -20) {
			//game.eliminarDisparo(this);
		//}
	}
	
	/**
	 * Notificacion de que esta entidad ha chocado con otra
	 * 
	 * @parma other La entidad que hemos chocado
	 */
	public void collidedWith(Objeto other) {
		//Si ya chocamos con algo, no hay que volver a comprabarlo.
		if (used) {
			return;
		}

		game.eliminarDisparo(this);
		other.resolverColision();
			
		used = true;
	}
	
	/**
	 * Comprueba si hubo colision con otro objeto
	 * 
	 * @param otrher El objeto a comprobar
	 */
	public boolean collidesWith(Objeto other) {
		return me.intersects(other.getBounds());
	}
}