package Logica.Entidades;

/**
 * Entidad que repesenta un Objeto normal.
 * Los objetos normales no son Destructibles.
 * El Robot no puede transitar sobre Objetos.
 * Todos los Objetos pueden responder a eventos
 * externos.
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public class Objeto extends Entidad{
	
	/** Tipo de Objeto (Concreto) */
	protected String tipo;
	/** Piso en el que se encuentra este Objeto */
	protected Piso piso;
	/** Referencia al Robot del juegador*/
	//protected Robot robot;
	
	public Objeto(Piso p, String ref, int x,int y, String tipo) {
		super(ref, x, y);
		this.tipo = tipo;
		//robot = r;
		piso = p;
	}
		
	/** Retorna el tipo del Objeto.
	 *
	 * @return tipo Tipo del Objeto
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * Setea el piso donde se encuentra este objeto
	 * 
	 * @param p Piso
	 */
	public void setPiso(Piso p) {
		piso = p;
	}
	
	/**
	 * Logica para resolver colisiones
	 * 
	 */
	public void resolverColision(){
		
	}
	/**
	 * Eventos que puede producir este Objeto en respeusta
	 * a otros eventos.
	 * 
	 * @param r Robot que produce el evento
	 */
	public void dispararEvento(Robot r) {
		
	}
}
