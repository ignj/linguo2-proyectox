package Logica.Entidades;

/**
 * Entidad que representa un Premio del juego.
 * Los premios pueden contener Nafta, Balas o Puntaje.
 * Estos estan contenidos en los Objetos Destructibles.
 * Al ser liberados pueden ser agarrados por el Robot.
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public class Premio extends Objeto {
	
	/** Cantidad del contenido*/
	protected int cantidad;
	
	public Premio(Piso p, String ref,int x,int y, String tipo, int cant) {
		super(	p, ref, x, y, tipo);
		cantidad = cant;
		//robot = r;
	}

	/**
	 * Otorga al Robot un Premio
	 * 
	 * @param premio Premio a recibir
	 */
	public void dispararEvento(Robot robot) {
		switch (getTipo()) {
			case "nafta": robot.addNafta(10);
			break;
			case "puntos": robot.addPuntaje(100);
			break;
			case "balas": robot.addAccionEspecial(10);
			break;
		}
		piso.setObjeto(null);
	}
	
}
