package Logica.Entidades;


/**
 * Entidad que representa un Objeto Destructible del Juego.
 * Los Objetos Destructibles pueden ser da�ados por el Robot Militar
 * Al ser destruidos dejan un Premio al Azar.
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public class Destructible extends Objeto {

	/** Indice de Durabilidad del Destructible*/
	protected int durabilidad;
	
	/** 
	 * Constructor
	 */
	public Destructible(Piso p, String ref,int x,int y, String tipo, int durabilidad) {
		super(p, ref, x, y, tipo);
		this.durabilidad = durabilidad;
	}
	
	/**
	 * Resuelve la colision de una entidad con un Destructible
	 * Da�ando al Destructible
	 */
	public void resolverColision(){
		recibirDa�o();
	}
	
	/** 
	 * Actualiza el indice de durabilidad del objeto al recibir da�o.
	 * En caso de reducir el indice a cero, se autodestruye.
	 * 
	 */
	public void recibirDa�o() {
		durabilidad--;
		if(durabilidad <= 0)
			destruir();
	}
	
	/** 
	 * Simula la destruccion del objeto.
	 * Deja un Premio al Azar en su lugar.
	 * 
	 */
	public void destruir() {
		//Quitamos la referencia al Destructible del Piso.
		//Creamos un premio y lo colocamos en el Piso.
		piso.setObjeto(premioAlAzar());
	}
	
	/** Generamos un nuevo Premio aleatorio */
	private Premio premioAlAzar() {
		Premio p = null;
		//Random [0,2]
		switch (0 + (int)(Math.random() * ((2 - 0) + 1))) {
			case 0: p = new Premio(piso, "nafta.png", (int)x, (int)y, "nafta", 10);
			break;
			case 1: p = new Premio(piso, "polvora.png", (int)x, (int)y, "balas", 10);
			break;
			case 2: p = new Premio(piso, "gold.png", (int)x, (int)y, "puntos", 100);
			break;
		}
		return p;
	}
	
	/** 
	 * Retorna el indice de durabilidad actual del Objeto
	 * 
	 * @returns durabilidad Indice de durabilidad actual
	 */
	public int getDurabilidad() {
		return durabilidad;
	}
}
