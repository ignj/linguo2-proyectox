package Logica.Textures;

/**
 * Manejador de Sprites del juego.
 * Cuenta con el Manejador de Skins, al cual le pide los Sprites.
 * Puede utilizar diferentes Skins con tan solo crear una nueva
 * instancia de SkinManager con el nombre apropiado.
 * <p>
 * [Utiliza el patron de Dise�o Singleton]
 * <p>
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public class SpriteManager {
	
	/** The single instance of this class */
	private static SpriteManager instancia = new SpriteManager();
	
	/**
	 * Get the single instance of this class 
	 * 
	 * @return The single instance of this class
	 */
	public static SpriteManager get() {
		return instancia;
	}
	
	/** The cached sprite map, from reference to sprite instance */
	//private HashMap sprites = new HashMap();
	private SkinManager sprites = new DefaultSkin();
	
	public Sprite getSprite(String ref) {
		return sprites.get(ref);
	}
	
	public void changeSkin(SkinManager skin) {
		sprites = skin;
	}
	
	public SkinManager getSkin() {
		return sprites;
	}
}