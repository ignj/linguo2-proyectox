package Logica.Textures;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

/**
 * 
 * Un Sprite a ser dibujado en la pantalla.
 * Mantiene la imagen en memoria para ser utilizada cuantas 
 * veces sea nesesario.
 * Solo existe una copia de cada imagen.
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public class Sprite {
	/** The image to be drawn for this sprite */
	public BufferedImage image;
	
	/**
	 * Create a new sprite based on an image
	 * 
	 * @param image The image that is this sprite
	 */
	public Sprite(BufferedImage image) {
		this.image = image;
	}
	
	/**
	 * Get the width of the drawn sprite
	 * 
	 * @return The width in pixels of this sprite
	 */
	public int getWidth() {
		return image.getWidth(null);
	}

	/**
	 * Get the height of the drawn sprite
	 * 
	 * @return The height in pixels of this sprite
	 */
	public int getHeight() {
		return image.getHeight(null);
	}
	
	/**
	 * Draw the sprite onto the graphics context provided
	 * 
	 * @param g The graphics context on which to draw the sprite
	 * @param x The x location at which to draw the sprite
	 * @param y The y location at which to draw the sprite
	 */
	public void draw(Graphics g,int x,int y) {
		g.drawImage(image,x,y,null);
	}
	
	/**
	 * Rota la imagen
	 * 
	 * @param angle Angulo de rotacion
	 */
	public void drawRotated(Graphics g, int x, int y, double angle){
		// The required drawing location
		int drawLocationX = x;
		int drawLocationY = y;

		// Rotation information
		double rotationRequired = Math.toRadians(angle);
		double locationX = image.getWidth(null) / 2;
		double locationY = image.getHeight(null) / 2;
		AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);

		// Drawing the rotated image at the required drawing locations
		g.drawImage(op.filter((BufferedImage) image, null), drawLocationX, drawLocationY, null);
	}
	
	/**  
	 * Cambia el tama�o de la imagen antes de ser dibujada
	 * Por razones de eficiencia no lo use mas.
	 * 
	 */
	public void drawScaled(Graphics g, int x, int y, double fWidth, double fHeight) {
	    //dbi = new BufferedImage(dWidth, dHeight, imageType);
	    AffineTransform at = AffineTransform.getScaleInstance(fWidth, fHeight);
	    AffineTransformOp op = new AffineTransformOp(at, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
	    
	    g.drawImage(op.filter((BufferedImage) image, null), x, y, null);
	    //((Graphics2D) g).drawRenderedImage((RenderedImage) image, at);
	}
	
	public void colorImage(int r, int g, int b) {
        int width = image.getWidth();
        int height = image.getHeight();
        WritableRaster raster = image.getRaster();

        for (int xx = 0; xx < width; xx++) {
            for (int yy = 0; yy < height; yy++) {
                int[] pixels = raster.getPixel(xx, yy, (int[]) null);
                pixels[0] = r;
                pixels[1] = g;
                pixels[2] = b;
                raster.setPixel(xx, yy, pixels);
            }
        }
    }
}