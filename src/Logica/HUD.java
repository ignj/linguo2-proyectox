package Logica;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import Logica.Entidades.Robot;
import Logica.Textures.Sprite;
import Logica.Textures.SpriteManager;

/** 
 * HUD (Heads Up Display)
 * Muestra informacion sobre el estado actual del Robot.
 * Nivel de Nafta, Municion en el Robot Militar, Contenido del Baul
 * Puntaje actual. Tipo de Robot y una Imagen de el mismo. 
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public class HUD {

	protected Robot robot;
	protected Sprite nafta; //40x40
	/** Posicion actual en x */  
	protected double cx;
	/** Posicion actual en y */
	protected double cy;
	
	private Sprite balas; //40x40
	/** Posicion actual en x */  
	protected double bx;
	/** Posicion actual en y */
	protected double by;
	
	private Sprite baul; //40x40
	/** Posicion actual en x */  
	protected double xx;
	/** Posicion actual en y */
	protected double yy;
	
	private Sprite puntaje;
	/** Posicion actual en x */  
	protected double px;
	/** Posicion actual en y */
	protected double py;
	
	/** Posicion actual en x */  
	protected double rx;
	/** Posicion actual en y */
	protected double ry;
	
	public HUD(Robot robot) {
		this.robot = robot;
	}
	
	/**
	 * Actualiza la imagen de la Nafta
	 * 
	 * @param ref String de nombre de imagen
	 * @param x Pos x
	 * @param y Pos y
	 */
	public void setNafta(String ref, int x, int y) {
		this.nafta = SpriteManager.get().getSprite(ref);
		this.cx = x;
		this.cy = y;
	}
	/**
	 * Actualiza la imagen de las Balas
	 * 
	 * @param ref String de nombre de imagen
	 * @param x Pos x
	 * @param y Pos y
	 */
	public void setBalas(String ref, int x, int y) {
		this.balas = SpriteManager.get().getSprite(ref);
		this.bx = x;
		this.by = y;
	}
	/**
	 * Actualiza la imagen del Puntaje
	 * 
	 * @param ref String de nombre de imagen
	 * @param x Pos x
	 * @param y Pos y
	 */
	public void setPuntaje(String ref, int x, int y) {
		puntaje = SpriteManager.get().getSprite(ref);
		this.px = x;
		this.py = y;
	}
	/**
	 * Actualiza la imagen del Baul
	 * 
	 * @param ref String de nombre de imagen
	 * @param x Pos x
	 * @param y Pos y
	 */
	public void setBaul(String ref, int x, int y) {
		this.baul = SpriteManager.get().getSprite(ref);
		this.xx = x;
		this.yy = y;
	}

	/**
	 * Dibuja los graficos
	 * 
	 * @param g Contexto Grafico
	 */
	public void draw(Graphics g) {
		g.setColor(Color.white);
		g.setFont( new Font( "SansSerif", Font.BOLD, 24 ) );
	
		//Nafta
		nafta.draw(g,(int) cx,(int) cy);
		g.drawString(robot.getNafta()+"", (int)cx+40, (int)cy+30);
		
		//Puntos
		puntaje.draw(g,(int) px,(int) py);
		g.drawString(robot.getPuntaje()+"", (int)px+45, (int)py+30);//(g,(int) cx,(int) cy);
		
		//Baul
		baul.draw(g,(int) xx,(int) yy);
		g.drawString("x"+robot.getCantColeccionables(), (int)xx+40, (int)yy+30);
		
		robot.getHUDicon().draw(g,(int) rx,(int) ry);
		if (robot.tieneAccionEspecial()) {
			balas.draw(g,(int) bx,(int) by);
			g.drawString(robot.accionEspecial()+"", (int)bx+40, (int)by+30);
		}
	}
}
