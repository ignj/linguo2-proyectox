package CrearNiveles;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import Logica.Textures.Sprite;
import Logica.Textures.SpriteManager;

/** 
 * Ayuda del juego.
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public class HelpEditor {
	
	protected int paginaActual;
	
	protected Sprite map;
	protected int mapX;
	protected int mapY;
	
	protected String title = "Linguo 2 Editor";
	//protected String subTitle = "The Empire Strike Back on the Lost Ark of the Temple of Doom, in a New Hope";
	
	protected String linea1 = "Los controles son basicos:";
	
	protected String linea2 = "Con las flechas de direcciones se mueve por el mapa";
	protected String linea3 = "Con la tecla SPACE se pone un piso en el mapa";
	
	protected String linea4 = "Con la tecla ENTER se obtiene el piso sobre el cual se esta parado";
	
	protected String linea5 = "Presionando la letra G se puede guardar el mapa";
	protected String linea6 = "Con la tecla M se puede detener la musica";
	
	protected String linea7 = "Nota: solo se permite un solo robot por mapa";
	
	protected String linea8 = "Presione F1 para salir de la ayuda";
	
	public HelpEditor(String ref){
		
		paginaActual = 0;
		
		map = SpriteManager.get().getSprite(ref);
		mapX = (800-map.getWidth())/2;
		mapY = (600-map.getHeight())/2;
		
	}
	
	private int getTextHorizontalPos(Graphics g, String text, int x) {
		return (x-g.getFontMetrics().stringWidth(text))/2;
	}
	
	public void draw(Graphics g) {
		
		g.setColor(Color.black);
		
		//Dibujo el Fondo
		map.draw(g, mapX, mapY);
		
		//Fuente del Titulo
		g.setFont( new Font( "ManoSpaced", Font.BOLD, 24 ) );
		//Dibujo el Titulo
		g.drawString(title,getTextHorizontalPos(g, title, 800), 70);
		
		//Fuente del subTitulo
		g.setFont( new Font( "ManoSpaced", Font.BOLD, 14 ) );
		
		g.setFont( new Font( "ManoSpaced", Font.PLAIN, 14 ) );
		
		//Linea 1
		g.drawString(linea1,getTextHorizontalPos(g, linea1, 800), 120);
		
		
		//Linea 2
		g.drawString(linea2,getTextHorizontalPos(g, linea2, 800), 155);
		//Linea 3
		g.drawString(linea3,getTextHorizontalPos(g, linea3, 800), 175);
		
		//Linea 4
		g.drawString(linea4,getTextHorizontalPos(g, linea4, 800), 195);
		
		//Linea 5
		g.drawString(linea5,getTextHorizontalPos(g, linea5, 800), 255);
		//Linea 6
		g.drawString(linea6,getTextHorizontalPos(g, linea6, 800), 270);
		
		//Linea 7
		g.drawString(linea7,getTextHorizontalPos(g, linea7, 800), 290);
		
		
		//Linea 8
		g.drawString(linea8,getTextHorizontalPos(g, linea8, 800), 350);
	}
}
