package CrearNiveles;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;




import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Logica.AudioPlayer;

/**
 * Interfaz grafica del editor de niveles
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
@SuppressWarnings("serial")
public class GUI extends Canvas{
	
	/**
	 * Valores de los pisos
	 */
	protected PisoEdicion arena;
	protected PisoEdicion asfalto;
	protected PisoEdicion agua;
	protected PisoEdicion fuego;
	protected PisoEdicion robotArena;
	protected PisoEdicion objetivoArena;
	protected PisoEdicion plantaArena;
	protected PisoEdicion cajaArena;
	protected PisoEdicion paredArena;
	protected PisoEdicion naftaArena;
	protected PisoEdicion balasArena;
	protected PisoEdicion puntosArena;
	protected PisoEdicion balizaArena;
	protected PisoEdicion conoTransitoArena;
	protected PisoEdicion robotAsfalto;
	protected PisoEdicion objetivoAsfalto;
	protected PisoEdicion plantaAsfalto;
	protected PisoEdicion cajaAsfalto;
	protected PisoEdicion paredAsfalto;
	protected PisoEdicion naftaAsfalto;
	protected PisoEdicion balasAsfalto;
	protected PisoEdicion puntosAsfalto;
	protected PisoEdicion balizaAsfalto;
	protected PisoEdicion conoTransitoAsfalto;
	
	
	/** Distancia del borde la ventana en la que empieza a dibujar */
	private final int OFFSET = 0;//30;
	/** Espacio entre los bloques que componen el nivel */
    private final int SPACE = 40;//20;
    
    /** Verdadero si el juego esta iniciando */
    private boolean startUp = true;
    /** Verdadero si el juego esta corriendo */
    private boolean gameRunning = true;
    /** Strategy que utiliza el Buffer para hacer uso de Accelerated Page Flipping */
    private BufferStrategy strategy;
    /** La ultima vez que calculamos el fps */
    private long lastFpsTime;
    /** Numero actual de cuadros por segundo */
    private int fps;
    /** Referencia a la Ayuda del juego */
	private HelpEditor help;
	/** Verdadero si la Ayuda esta visible */
	private boolean toggleHelp = false;
	/** Referencia a la Fuente de Audio principal */
    private AudioPlayer audioSource;  
    /** verdadero si la musica se reproduce*/
    private boolean reproducirMusica = true;
    /** directorio del archivo de musica*/
    private String directorioArchivoMusica;
    
	
	/** Verdadero si el juego esta detenido hasta el ingreso de una tecla */
	private boolean waitingForKeyPress = true;
	/** Verdadero si la tecla ARRIBA esta siendo presionada */
	private boolean upPressed = false;
	/** Verdadero si la tecla ABAJO esta siendo presionada */
	private boolean downPressed = false;
	/** Verdadero si la tecla IZQUIERDA esta siendo presionada */
	private boolean leftPressed = false;
	/** Verdadero si la tecla DERECHA esta siendo presionada */
	private boolean rightPressed = false;
	/** Verdadero si la tecla ESPACIO esta siendo presionada */
	private boolean ponerPiso = false;
	/** verdadero si se presiono el boton para guardar el nivel*/
	private boolean guardarNivel = false;
	/** verdadero si se presiono la tecla ENTER para tomar los datos de un piso**/
	private boolean tomarDatosPiso = false;

	/** Nombre de la ventana */
	private String windowTitle = "Linguo 2 Editor de mapas";
	/** La ventana a la cual se le actualiza el FPS */
    private JFrame container;
    
    /** Matriz que representa el Nivel actual */
    protected PisoEdicion[][] Nivel;
    /** El Piso Objetivo del juego */
    protected PisoEdicion pisoObjetivo;   
    /** Posicion en X del puntero en la Matriz del Nivel (FILA) */
    protected int posFil;
	/** Posicion en Y del puntero en la Matriz del Nivel  (COLUMNA)*/
    protected int posCol;	
    /** cantidad total de columnas en la matriz */
    protected final int tamaņoCol = 22;
    /** cantidad total de filas en la matriz */
    protected final int tamaņoFila = 15;
    /** cantidad total de columnas que van a formar el mapa en la matriz */
    protected final int columnasEfectivas = 20;
    /** cantidad total de filas que van a formar el mapa en la matriz */
    protected final int filasEfectivas = 15;
    /** verdadero si hay un robot en el mapa */
    protected boolean hayRobotEnMapa = false;
    
    
	public static void main(String argv[]) {
		
    	GUI ex = new GUI();
        ex.gameLoop();

	}
	
	/**
	 * da las caracteristicas principales de la interfaz grafica
	 */
	public GUI(){
		// Crea un Frame donde contener el juego
        container = new JFrame("Linguo 2 Editor");
        
        // Resolucion del juego
        JPanel panel = (JPanel) container.getContentPane();
        panel.setPreferredSize(new Dimension(880,600));
        panel.setLayout(null);
        
        // Setea el Canvas y lo coloca en el Panel del JFrame
        setBounds(0,0,880,600);
        panel.add(this);

        // Dice a AWT que no se preocupe por repintar el canvas
        // ya que lo haremos nosotros en modo accelerado
        setIgnoreRepaint(true);
        
        // hacer la ventana visible
        container.pack();
        container.setResizable(false);
        container.setVisible(true);
        
        // Agrega un oyente para responder al cierre de la ventana
        container.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                        System.exit(0);
                }
        	}
        );
        
        // Agrega el sistema de input (Clase anidada abajo) al canvas
        // para pdoer responder al input del teclado
        addKeyListener(new KeyInputHandler());
        
        // pide foco para que el input del teclado venga aca
        requestFocus();
        // crea el strategy del buffer que permite a AWT
        // manejar graficos accelerados
        createBufferStrategy(2);
        strategy = getBufferStrategy();
        
        //Crea la ayuda con su fondo
      	help = new HelpEditor("help.png");
        //Crea la fuente de sonido
      	directorioArchivoMusica = "Data/Audio/pfeos.ogg";
      	audioSource = new AudioPlayer(directorioArchivoMusica);
      	//Inicia la musica de fondo
      	audioSource.startMusic(0);
        
        reiniciarNivel();

	}
	
	/**
     * Reinica el Nivel
     */
    private void reiniciarNivel() {
            if (Nivel != null) limpiarNivel();
            initWorld();            
            upPressed = false;
            leftPressed = false;
            rightPressed = false;
            ponerPiso = false;
            posFil=0;
            posCol=0;
       
    }
    
    /**
     *  Limpia la Matriz del Nivel
     */
    public void limpiarNivel() {
            for (int i=0;i<Nivel.length;i++) {
                    for(int j=0;j<Nivel[i].length;j++) {
                            Nivel[i][j] = null;
                    }
            }
    }
    
    /**
     * Inicializa la Matriz del Nivel.
     * Lee los datos del nivel y los usa para cargar la Matriz
     */
    public void initWorld() {
    
	    int x = OFFSET;
	    int y = OFFSET;

	    Nivel = new PisoEdicion[tamaņoFila][tamaņoCol];
	    
	    inicializarPisosPorDefecto();
	    
	    posFil=0;
	    posCol=0;
	    pisoObjetivo = new PisoEdicion("arena.png",posFil,posCol,'o',"objetivo.png");
	    pisoObjetivo.ponerBorde();
        
        for (int fila=0; fila<tamaņoFila; fila++){
        	for (int columna=0; columna<tamaņoCol; columna++){
        		if (posicionValidaDeEscritura(fila,columna)){
        			PisoEdicion nuevo = arena.clone();
        			nuevo.setX(x);
        			nuevo.setY(y);
        			Nivel[fila][columna] = nuevo;
        		}
        		x+=SPACE;
        	}
        	y+=SPACE;
        	x=OFFSET;
    	}
	}
    
    /**
     * este loop corre durante toda la ejecuccion del juego.
     * <p>
     * - Maneja la velocidad del ciclo
     * - Mueve las entidades
     * - Dibuja los graficos
     * - Actualiza los eventos
     * - Comprueba el input
     * <p>
     */
    public void gameLoop() {
    	long lastLoopTime = System.currentTimeMillis();        
        
        //Ciclar hasta el fin del mundo
        while (gameRunning) {
	        	// Calcular cuanto tiempo transcurrio desde el ultimo Update.
	            // Se utiliza para saber cuento hay que mover a las entidades
	            
	            long delta = System.currentTimeMillis() - lastLoopTime;
	            lastLoopTime = System.currentTimeMillis();
	
	            // actualiza el contador de frames (cuadros)
	            lastFpsTime += delta;
	            fps++;
	            
	            // actualiza el FPS (cuadros por segundo) si un segundo a transcurrido
	            if (lastFpsTime >= 1000) {
	                    container.setTitle(windowTitle+" (FPS: "+fps+")");
	                    lastFpsTime = 0;
	                    fps = 0;
	            }
        	
                // Obtener el contexto Grafico y limpiarlo
                Graphics2D g = (Graphics2D) strategy.getDrawGraphics();
                g.setColor(Color.blue);
                g.fillRect(0,0,880,600);
                
                if (!waitingForKeyPress) {
                    // Recorre la Matriz del Nivel dibujando las Entidades.
                    for (int i=0;i<Nivel.length;i++) {
                            for(int j=0;j<Nivel[i].length;j++) {
                                    PisoEdicion piso = Nivel[i][j];
                                    piso.draw(g);
                            }
                    }
                    //DIBUJO EL PUNTERO                    
                    pisoObjetivo.draw(g);
                }
                if (waitingForKeyPress) {                
                    g.setColor(Color.WHITE);
                    
                    if(startUp) {
                            g.setFont( new Font( "SansSerif", Font.BOLD, 72 ) );
                    
                            g.drawString("LINGUO 2 EDITOR",(880-g.getFontMetrics().stringWidth("LINGUO 2 EDITOR"))/2,200);
                            String subtitle = "PRESIONAR F1 PARA AYUDA";
                            g.setFont( new Font( "SansSerif", Font.BOLD, 20 ) );
                            g.drawString(subtitle,(880-g.getFontMetrics().stringWidth(subtitle))/2,240);
                            g.drawString("Creditos",(880-g.getFontMetrics().stringWidth("Creditos"))/2,400);
                            g.setFont( new Font( "SansSerif", Font.BOLD, 30 ) );
                            g.drawString("Ignacio del Barrio",(880-g.getFontMetrics().stringWidth("Ignacio del Barrio"))/2,440);
                            g.drawString("Ignacio Jocano",(880-g.getFontMetrics().stringWidth("Ignacio Jocano"))/2,480);
                            g.drawString("Alexis Aguilera",(880-g.getFontMetrics().stringWidth("Alexis Aguilera"))/2,520);                            
                    }
                    
                    g.setFont( new Font( "SansSerif", Font.BOLD, 36 ) );
                    g.drawString("PRESIONE UNA TECLA",(880-g.getFontMetrics().stringWidth("PRESIONE UNA TECLA"))/2,300);
                }                
                
                if (!waitingForKeyPress) {    			    				
    				//Dibujamos el menu de ayuda
    				if(toggleHelp) help.draw(g);
    			}
                
                // terminamos de dibujar. Tiramos los graficos
                g.dispose();
                strategy.show();
                
                //Movimiento del puntero
                
                if ((leftPressed) && (!rightPressed) && (!upPressed) && (!downPressed) && (posCol>0)) {
                        pisoObjetivo.setX(pisoObjetivo.getX()-SPACE);
                        posCol-=1;
                } else if ((rightPressed) && (!leftPressed) && (!upPressed) && (!downPressed) && (posCol<(Nivel[posFil].length-1))) {
                		pisoObjetivo.setX(pisoObjetivo.getX()+SPACE);
                		posCol+=1;
                } else if ((!rightPressed) && (!leftPressed) && (upPressed) && (!downPressed) && (posFil>0)) {
                		pisoObjetivo.setY(pisoObjetivo.getY()-SPACE);
                		posFil-=1;
                } else if ((!rightPressed) && (!leftPressed) && (!upPressed) && (downPressed) && (posFil<(Nivel.length-1))) {
                		pisoObjetivo.setY(pisoObjetivo.getY()+SPACE);
                		posFil+=1;
                }
                //SI LA POSICION ES VALIDA EL PISO SE MODIFICA
                if (ponerPiso && posicionValidaDeEscritura(posFil,posCol)){    
                	if (esRobot(pisoObjetivo) && hayRobotEnMapa())
                		if (esRobot(Nivel[posFil][posCol]))
                			Nivel[posFil][posCol] = pisoObjetivo.cloneProfundo();
                		else{                			
                			apagarMovimientos();
                			JOptionPane.showMessageDialog(null,"Solo se permite un robot por mapa","Advertencia", JOptionPane.WARNING_MESSAGE);
                			ponerPiso=false;
                		}
                	else if (esObjetivo(pisoObjetivo) && hayObjetivoEnMapa()){
                		if (esObjetivo(Nivel[posFil][posCol]))
                			Nivel[posFil][posCol] = pisoObjetivo.cloneProfundo();
                		else{
                			apagarMovimientos();
                			JOptionPane.showMessageDialog(null,"Solo se permite un objetivo por mapa","Advertencia", JOptionPane.WARNING_MESSAGE);
                			ponerPiso=false;
                		}
                	}
                		else{                		
                			Nivel[posFil][posCol] = pisoObjetivo.cloneProfundo();//new PisoEdicion(pisoObjetivo.getNombreSprite(),pisoObjetivo.getX(),pisoObjetivo.getY());                		
                		}
                		
                }
                //Se toman los datos del piso sobre el que se esta parado
                if (tomarDatosPiso){
                	pisoObjetivo= Nivel[posFil][posCol].cloneProfundo();
                	pisoObjetivo.ponerBorde();
                }
                
                // Comprobar si se quiere guardar el nivel
                if(guardarNivel){
                		String nombre = JOptionPane.showInputDialog(null,"Ingrese nombre del mapa","Guardar",1);
                		if (nombre!=null){
	                		char[][] matrizTipos = interpretarNiveles();
	                		InterfazCrearMapa var = new CrearMapaEnArchivo(nombre,matrizTipos);
	                        try {
								var.guardarMapa();
							} catch (ErrorAlGuardarMapa e) {
								JOptionPane.showMessageDialog(null,e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE); 
							}
                		}
                        guardarNivel=false;
                }
                
                if (reproducirMusica){
                	if (audioSource.isStopped(0))
                		audioSource.startMusic(0);
                }
                else{
                	if (!audioSource.isStopped(0))
                		audioSource.stopMusic(0);
                }
                
                
                try { 
                	Thread.sleep(lastLoopTime+100-System.currentTimeMillis());
                	//Thread.sleep(100);
                	}
                catch (Exception e) {} //20
        
        }
    }
    
    private class KeyInputHandler extends KeyAdapter {
        /** The number of key presses we've had while waiting for an "any key" press */
        private int pressCount = 1;
        
        /**
         * Notification from AWT that a key has been pressed. Note that
         * a key being pressed is equal to being pushed down but *NOT*
         * released. Thats where keyTyped() comes in.
         *
         * @param e The details of the key that was pressed 
         */
        public void keyPressed(KeyEvent e) {
                // if we're waiting for an "any key" typed then we don't 
                // want to do anything with just a "press"
                if (waitingForKeyPress) {
                        return;
                }
                
                
                if (e.getKeyCode() == KeyEvent.VK_UP) {
                        upPressed = true;
                }
                if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                        downPressed = true;
                }
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                        leftPressed = true;
                }
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                        rightPressed = true;
                }
                if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                        ponerPiso = true;
                }
                if (e.getKeyCode() == KeyEvent.VK_ENTER){
                		tomarDatosPiso = true;
                }
                //GUARDAR NIVEL
                if (e.getKeyCode() == KeyEvent.VK_G)
                	guardarNivel = true;                                
                //Ayuda con F1
                if (e.getKeyCode() == KeyEvent.VK_F1) {
                        toggleHelp = !toggleHelp;
                }  
                //Prender/apagar musica
                if (e.getKeyCode() == KeyEvent.VK_M){
                	reproducirMusica = reproducirMusica? false : true;
                }
                          
        } 
        
        /**
         * Notification from AWT that a key has been released.
         *
         * @param e The details of the key that was released 
         */
        public void keyReleased(KeyEvent e) {
                // if we're waiting for an "any key" typed then we don't 
                // want to do anything with just a "released"
                if (waitingForKeyPress) {
                        return;
                }
                
                if (e.getKeyCode() == KeyEvent.VK_UP) {
                        upPressed = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                        downPressed = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                        leftPressed = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                        rightPressed = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                        ponerPiso = false;
                }
                if (e.getKeyCode() == KeyEvent.VK_ENTER){
            			tomarDatosPiso = false;
                }                
                
        }

        /**
         * Notification from AWT that a key has been typed. Note that
         * typing a key means to both press and then release it.
         *
         * @param e The details of the key that was typed. 
         */
        public void keyTyped(KeyEvent e) {
                // if we're waiting for a "any key" type then
                // check if we've recieved any recently. We may
                // have had a keyType() event from the user releasing
                // the shoot or move keys, hence the use of the "pressCount"
                // counter.
                if (waitingForKeyPress) {
                        if (pressCount == 1) {
                                // since we've now recieved our key typed
                                // event we can mark it as such and start 
                                // our new game
                                waitingForKeyPress = false;
                                startUp = false;
                                reiniciarNivel();
                                pressCount = 0;
                        } else {
                                pressCount++;
                        }
                }       
                // if we hit escape, then quit the game
                if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
                        System.exit(0);
                }
        }
    }
    
    /**
     * inicializa las variables globales de los pisos
     */
    public void inicializarPisosPorDefecto(){
    	//PISOS BASE
    	arena= new PisoEdicion("arena.png",0,0,'a');
    	Nivel[0][0]= arena;
    	asfalto= new PisoEdicion("asfalto.png",40,0,'s');
    	Nivel[0][1]= asfalto;
    	agua= new PisoEdicion("agua.png",0,40,'g');
    	Nivel[1][0]= agua;
    	fuego= new PisoEdicion("fuego.png",40,40,'f');
    	Nivel[1][1]= fuego;
    	paredArena = new PisoEdicion("concreto.png",0,80,'d');
    	Nivel[2][0] = paredArena;
    	
    	//OBJETOS SOBRE ARENA
    	objetivoArena= new PisoEdicion("arena.png",40,80,'o',"objetivo.png");
    	Nivel[2][1]= objetivoArena;
    	plantaArena= new PisoEdicion("arena.png",0,120,'p',"planta.png");
    	Nivel[3][0]= plantaArena;
    	cajaArena= new PisoEdicion("arena.png",40,120,'c',"caja.png");
    	Nivel[3][1]= cajaArena;
    	robotArena = new PisoEdicion("arena.png",0,160,'r',"robot.png");
    	Nivel[4][0] = robotArena;
    	naftaArena= new PisoEdicion("arena.png",40,160,'n',"nafta.png");
    	Nivel[4][1]= naftaArena;
    	balasArena= new PisoEdicion("arena.png",0,200,'b',"polvora.png");
    	Nivel[5][0]= balasArena;
    	puntosArena= new PisoEdicion("arena.png",40,200, 't',"gold.png");
    	Nivel[5][1]= puntosArena;
    	balizaArena= new PisoEdicion("arena.png",0,240,'z',"baliza.png");
    	Nivel[6][0]= balizaArena;
    	conoTransitoArena= new PisoEdicion("arena.png",40,240,'i',"cono.png");
    	Nivel[6][1]= conoTransitoArena;
    	
    	//OBJETOS SOBRE ASFALTO
    	objetivoAsfalto= new PisoEdicion("asfalto.png",0,280,'O',"objetivo.png");
    	Nivel[7][0]= objetivoAsfalto;
    	plantaAsfalto= new PisoEdicion("asfalto.png",40,280,'P',"planta.png");
    	Nivel[7][1]= plantaAsfalto;
    	cajaAsfalto= new PisoEdicion("asfalto.png",0,320,'C',"caja.png");
    	Nivel[8][0]= cajaAsfalto;
    	robotAsfalto = new PisoEdicion("asfalto.png",40,320,'R',"robot.png");
    	Nivel[8][1] = robotAsfalto;
    	naftaAsfalto= new PisoEdicion("asfalto.png",0,360,'N',"nafta.png");
    	Nivel[9][0]= naftaAsfalto;
    	balasAsfalto= new PisoEdicion("asfalto.png",40,360,'B',"polvora.png");
    	Nivel[9][1]= balasAsfalto;
    	puntosAsfalto= new PisoEdicion("asfalto.png",0,400, 'T',"gold.png");
    	Nivel[10][0]= puntosAsfalto;
    	balizaAsfalto= new PisoEdicion("asfalto.png",40,400,'Z',"baliza.png");
    	Nivel[10][1]= balizaAsfalto;
    	conoTransitoAsfalto= new PisoEdicion("asfalto.png",0,440,'I',"cono.png");
    	Nivel[11][0]= conoTransitoAsfalto;
    	//pared = new PisoEdicion("pared.png",40, 440, '#');
    	Nivel[11][1]= new PisoEdicion("pared.png",40, 440, '#');
    	Nivel[12][0]= new PisoEdicion("pared.png",0, 480, '#');
    	Nivel[12][1]= new PisoEdicion("pared.png",40, 480, '#');
    	Nivel[13][0]= new PisoEdicion("pared.png",0, 520, '#');
    	Nivel[13][1]= new PisoEdicion("pared.png",40, 520, '#');
    	Nivel[14][0]= new PisoEdicion("pared.png",0, 560, '#');
    	Nivel[14][1]= new PisoEdicion("pared.png",40, 560, '#');
    }
    
    /**
     * determina si una celda es valida para poner un piso
     * @param fila fila que corresponde a la celda
     * @param columna columna que corresponde a la celta
     * @return verdadero si es una posicion valida, falso en caso contrario
     */
    public boolean posicionValidaDeEscritura(int fila, int columna){
    	if (columna<=1 && fila<=14)
    		return false;
    	else
    		return true;
    }
    
    /**
     * convierte una matriz de PisoEdicion en una matriz de caracteres
     * @return matriz de caracteres que refleja el mapa actual
     */
    public char[][] interpretarNiveles(){
    	char[][] mapaEnChar = new char[filasEfectivas][columnasEfectivas];
    	for (int fila=0; fila<filasEfectivas; fila++){    		
    		for (int col=0; col<columnasEfectivas; col++){
    			mapaEnChar[fila][col] = Nivel[fila][col+2].getTipo();    			
    		}
    	}
    	return mapaEnChar;
    }
    
    /**
     * determina si un piso contiene a un robot
     * @param p piso a analizar
     * @return verdadero si el piso contiene un robot, falso en caso contrario
     */
    public boolean esRobot(PisoEdicion p){
    	return p.getTipo()=='r' || p.getTipo()=='R';
    }
    
    /**
     * determina si ya se inserto un robot en el mapa
     * @return verdadero si hay un robot en el mapa, falso en caso contrario
     */
    public boolean hayRobotEnMapa(){
    	boolean hayRobot=false;
    	int fila=0;
    	int col=0;
    	while (fila<filasEfectivas && !hayRobot){
    		col=0;
    		while (col<columnasEfectivas && !hayRobot){
    			hayRobot = esRobot(Nivel[fila][col+2]);
    			col++;
    		}
    		fila++;
    	}
    	return hayRobot;
    }
    
    public boolean hayObjetivoEnMapa(){
    	boolean hayObjetivo=false;
    	int fila=0;
    	int col=0;
    	while (fila<filasEfectivas && !hayObjetivo){
    		col=0;
    		while (col<columnasEfectivas && !hayObjetivo){
    			hayObjetivo = esObjetivo(Nivel[fila][col+2]);
    			col++;
    		}
    		fila++;
    	}
    	return hayObjetivo;
    }
    
    public boolean esObjetivo(PisoEdicion p){
    	return p.getTipo()=='o' || p.getTipo()=='O';
    }
    
    /**
     * frena a todas las teclas que estan siendo presionadas.
     */
    public void apagarMovimientos(){
    	upPressed = false;
    	downPressed = false;
    	leftPressed = false;
    	rightPressed = false;
    }
        
}


