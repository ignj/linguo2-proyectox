package CrearNiveles;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * clase que se utiliza para guardar mapas en un archivo de texto
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 *
 */
public class CrearMapaEnArchivo extends InterfazCrearMapa {

	/** nombre con el que se va a guardar el mapa */
	protected String nombreMapa;
	/** matriz que contiene al mapa*/
	protected char[][] matrizTipos;
	
	/**
	 * crea un objeto CrearMapaEnArchivo con un nombre particular para el mapa
	 * @param nombreMapa nombre con el que se guarda el mapa
	 */
	public CrearMapaEnArchivo(String nombreMapa, char[][] matrizTipos){
		this.nombreMapa=nombreMapa+".txt";
		this.matrizTipos=matrizTipos;
	}
	
	/**
	 * crea un objeto CrearMapaEnArchivo y le da al mapa un nombre por defecto
	 */
	public CrearMapaEnArchivo(){
		nombreMapa="default.txt";
	}
	
	/**
	 * cambia el nombre con el que se va a guardar el archivo
	 * @param nom nombre con el que se reemplaza
	 */
	public void setNombreArchivo(String nom){
		nombreMapa=nom;
	}
	

	/**
	 * Guarda el mapa actual
	 * @throws ErrorAlGuardarMapa si se produce un error al guardar el mapa
	 */
	@Override
	public void guardarMapa() throws ErrorAlGuardarMapa{
        PrintWriter salida = null;
        int fila=0;
        int columna=0;
        try {
            salida = new PrintWriter(nombreMapa);                                    
            while (fila<matrizTipos.length) {
            	columna=0;
            	while (columna<matrizTipos[fila].length){
            		if (matrizTipos[fila][columna]=='\u0000') throw new ErrorAlGuardarMapa("El mapa tiene sectores vacios");
            		salida.print(matrizTipos[fila][columna]);
            		columna++;
            	}
            	if (fila!=matrizTipos.length) salida.println();
            	fila++;
            }
            salida.flush();
        }
        catch (FileNotFoundException e){
        	throw new ErrorAlGuardarMapa("No se pudo escribir el archivo");
        }
        finally{
        	salida.close();
        }
		
	}
	

}
