package CrearNiveles;

abstract public class InterfazCrearMapa {

	/**
	 * Guarda el mapa actual
	 * @throws ErrorAlGuardarMapa si se produce un error al guardar el mapa
	 */
	abstract public void guardarMapa() throws ErrorAlGuardarMapa;
	

}
