package ManejadorDeMapas;

import java.io.IOException;

@SuppressWarnings("serial")
public class ErrorAlCargarMapa extends IOException{
	
	/**
	 * Error que se devuelve al intentar cargar un mapa
	 * @param msj mensaje con el que se crea el error.
	 */
	
	public ErrorAlCargarMapa(String msj){
		super(msj);
	}

}
