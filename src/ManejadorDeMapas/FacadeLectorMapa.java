package ManejadorDeMapas;

abstract public class FacadeLectorMapa {
	
	/**
	 * Devuelve el contenido del archivo de texto pasado por parametro
	 * @param direccion direccion del archivo de texto
	 * @return string que representa al mapa del nivel
	 * @throws ErrorAlCargarMapa error que se produce al intentar manejar el archivo
	 */
	abstract public String cargarMapa(String direccion) throws ErrorAlCargarMapa;

}
